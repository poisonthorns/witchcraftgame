# WitchcraftGame



## What is??

Witchcraft is a Stardew Valley and Overcooked inspired game about making potions!


## Potential Upcoming Features

- [ ] Overcooked style potion brewing!
- [ ] Villagers and adventurers who make requests that adapt to your stock
- [ ] Familiars with distinct personalities that help you
- [ ] Farm for herbs, mine for precious metals, and fight monsters in the woods for important potion ingredients
- [ ] A spell system for combat?
